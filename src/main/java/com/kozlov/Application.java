/*
 * Copyright (c) 2019 Kozlov Artem.
 *This is the very first homework for Epam.
 * and here i print Fibonacci and user numbers.
 */
package com.kozlov;
import java.util.Scanner;
/**
 * Class for homework realisation task01_Fibonacci.
 * @version 1.1 31 March 2018
 * @author Artem Kozlov

 */
public  class Application {
    private static final int FIRST_FIBONACCI_NUMBER = 0;
    private static final int SECOND_FIBONACCI_NUMBER = 1;
    /**
     * method  reads all numbers for range and length of Fibonacci line.
     * Сhecks out if they are correct and call methods for processing
     * @param args are command line values
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in, "UTF-8");
        System.out.println("Please enter start of interval : ");
        int startOfInterval = scan.nextInt();
        System.out.println("Please enter end of interval : ");
        int endOfInterval = scan.nextInt();
        if (startOfInterval > endOfInterval) {
            System.out.println("End needs to be bigger than start, please enter new value");
            endOfInterval = scan.nextInt();
        }
        printEvenNumbers(startOfInterval, endOfInterval);
        printOddNumbers(startOfInterval, endOfInterval);
        System.out.println("Please enter length of Fibonacci line ");
        int lengthOfFibonacciLine = scan.nextInt();
        printFibonacciNumbersOrder(lengthOfFibonacciLine);
    }
    /**
     * method  prints out all even numbers from given range.
     * @param startOfInterval  start of users range
     * @param endOfInterval end of users range
     */
    private static void printEvenNumbers(int startOfInterval, int endOfInterval) {
        System.out.println("Even numbers are : ");
        for (int i = startOfInterval; i < endOfInterval + 1; i++) {
            if (i % 2 == 0) {
                System.out.print(" | " + i + " | ");
            }
        }
        System.out.println();
    }
    /**
     * method  prints out all odd numbers from given range.
     * Сalculates sum of odd and even numbers.
     * @param startOfInterval  start of users range
     * @param endOfInterval end of users range
     */
    private static void printOddNumbers(int startOfInterval, int endOfInterval) {
        int sum = 0;
        System.out.println("Odd numbers are : ");
        for (int i = endOfInterval; i > startOfInterval - 1; i--) {
            if (i % 2 != 0) {
                System.out.print(" | " + i + " | ");
            }
            sum += i;
        }
        System.out.println();
        System.out.println("sum = " + sum);
    }
    /**
     * method prints Fibonacci numbers from given range.
     * Сalculates percentage of each even/odd group
     * @param lengthOfLine is  length of Fibonacci line
     */
    private static void printFibonacciNumbersOrder(int lengthOfLine) {
        int[] arrayFibonacci = new int[lengthOfLine];
        arrayFibonacci[0] = FIRST_FIBONACCI_NUMBER;
        arrayFibonacci[1] = SECOND_FIBONACCI_NUMBER;
        int amountOfEvenNumbers = 1;
        int percentageOfEvenNumbers;
        int percentageOfOddNumbers;
        for (int j = 2; j < lengthOfLine; j++) {
            arrayFibonacci[j] = arrayFibonacci[j - 1] + arrayFibonacci[j - 2];
            if (arrayFibonacci[j] % 2 == 0) {
                amountOfEvenNumbers += 1;
            }
        }
        for (int j = 0; j < lengthOfLine; j++) {
            System.out.println(j + 1 + " number is " + " | " + arrayFibonacci[j] + " | ");
        }
        printBiggestFibonacciNumbers(arrayFibonacci);
        percentageOfEvenNumbers =  (amountOfEvenNumbers * 100) / lengthOfLine;
        percentageOfOddNumbers = 100 - percentageOfEvenNumbers;
        System.out.println("percentageOfEvenNumbers : " + percentageOfEvenNumbers);
        System.out.println("percentageOfOddNumbers : " + percentageOfOddNumbers);
    }
    /**
     *  method searches and prints the biggest odd number from given range and the biggest even one.
     * @param arrayFibonacci is all Fibonacci numbers for given range
     */
    private static void printBiggestFibonacciNumbers(int[] arrayFibonacci) {
        String evenFibonacci = "The biggest even number in Fibonacci line = ";
        String oddFibonacci = "The biggest odd number in Fibonacci line = ";
            if (arrayFibonacci[arrayFibonacci.length - 1] % 2 == 0) {
                System.out.println(evenFibonacci + arrayFibonacci[arrayFibonacci.length - 1]);
                System.out.println(oddFibonacci + arrayFibonacci[arrayFibonacci.length - 2]);
            } else {
                System.out.println(oddFibonacci + arrayFibonacci[arrayFibonacci.length - 1]);
                if (arrayFibonacci[arrayFibonacci.length - 2] % 2 == 0) {
                    System.out.println(evenFibonacci + arrayFibonacci[arrayFibonacci.length - 2]);
                } else {
                    System.out.println(evenFibonacci + arrayFibonacci[arrayFibonacci.length - 3]);
                }
            }
        }
    }


